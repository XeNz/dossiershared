using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Utilities.Correlations.Middleware;
using Utilities.Correlations.Providers;

namespace Utilities.Correlations
{
    public static class CorrelationExtensions
    {
        public static IServiceCollection AddCorrelations(this IServiceCollection services)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.TryAddTransient<ICorrelationProvider, CorrelationProvider>();
            return services;
        }

        public static IApplicationBuilder UseCorrelations(this IApplicationBuilder app)
        {
            if (app == null)
                throw new ArgumentNullException(nameof(app));

            app.UseMiddleware<CorrelationMiddleware>();
            return app;
        }
    }
}