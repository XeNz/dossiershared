using System;
using System.Runtime.Serialization;

namespace Utilities.Correlations.Model
{
    public class CorrelationContext
    {
        public Guid? Id { get; set; }
        public string SourceId { get; set; }
        public string SourceName { get; set; }
        public string InstanceId { get; set; }
        public string InstanceName { get; set; }
        public string UserId { get; set; }
        public string IpAddress { get; set; }

        [IgnoreDataMember] public string DgpHeader { get; set; }
    }
}