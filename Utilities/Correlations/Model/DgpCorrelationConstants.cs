namespace Utilities.Correlations.Model
{
    public static class DgpCorrelationConstants
    {
        public const string DgpCorrelationHeader = "Dgp-Correlation";
        public const string NarCorrelationId = "Nar-Correlation-Id";
    }
}