using System;
using Microsoft.AspNetCore.Http;
using Utilities.Correlations.Model;

namespace Utilities.Correlations.Providers
{
    public class CorrelationProvider : ICorrelationProvider
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CorrelationProvider(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public Guid GetCorrelationId()
        {
            var context = GetCorrelationContext();
            if (context.Id.HasValue)
                return context.Id.Value;

            throw new System.Exception(
                "Id was not found in the base64 string dgp_header, is the casing correct? no camelcase no correlation!");
        }

        public CorrelationContext GetCorrelationContext()
        {
            if (_httpContextAccessor.HttpContext.Items.TryGetValue(DgpCorrelationConstants.DgpCorrelationHeader,
                out var correlationContext))
                return correlationContext as CorrelationContext;

            throw new System.Exception(
                "Middleware was not registered! This value should always be filled from middleware");
        }
    }
}