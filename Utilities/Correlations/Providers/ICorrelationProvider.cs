using System;
using Utilities.Correlations.Model;

namespace Utilities.Correlations.Providers
{
    public interface ICorrelationProvider
    {
        Guid GetCorrelationId();
        CorrelationContext GetCorrelationContext();
    }
}