﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Digipolis.ApplicationServices;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using Utilities.Correlations.Model;

namespace Utilities.Correlations.Middleware
{
    public class CorrelationMiddleware
    {
        private readonly IApplicationContext _applicationContext;
        private readonly RequestDelegate _next;

        public CorrelationMiddleware(RequestDelegate next,
            IApplicationContext applicationContext)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next), $"{nameof(next)} cannot be null.");
            _applicationContext = applicationContext;
        }

        public async Task Invoke(HttpContext context)
        {
            var narCorrelationHeaderValue = context.Request?.Headers?.FirstOrDefault(x =>
                x.Key.Equals(DgpCorrelationConstants.NarCorrelationId, StringComparison.OrdinalIgnoreCase)).Value;
            var correlationHeaderValue = context.Request?.Headers?.FirstOrDefault(x =>
                x.Key.Equals(DgpCorrelationConstants.DgpCorrelationHeader, StringComparison.OrdinalIgnoreCase)).Value;
            CorrelationContext correlationContext = null;
            // if we have a correlation header value
            //try to get it from the header
            if (correlationHeaderValue.HasValue && correlationHeaderValue.Value.Any())
                correlationContext = GetCorrelationContextFromHeader(correlationHeaderValue.Value);

            // if no correlation id is given, try to get it from the nar correlation header
            if (correlationContext?.Id == null && narCorrelationHeaderValue.HasValue &&
                narCorrelationHeaderValue.Value.Any())
            {
                if (!Guid.TryParse(narCorrelationHeaderValue.Value.FirstOrDefault(), out var newCorrelationId))
                    newCorrelationId = Guid.NewGuid();

                correlationContext = CreateNewCorrelationContext(context, newCorrelationId);
            }
            //fallback, no or illegal correlation header is found and no nar correlation header is found, create a new one
            else if (correlationContext?.Id == null)
            {
                correlationContext = CreateNewCorrelationContext(context, Guid.NewGuid());
            }

            SerializeHeader(correlationContext);
            context.Items.Add(DgpCorrelationConstants.DgpCorrelationHeader, correlationContext);
            context.Items.Add(DgpCorrelationConstants.NarCorrelationId, correlationContext.Id.ToString());
            context.Response.Headers.Add(DgpCorrelationConstants.NarCorrelationId, correlationContext.Id.ToString());
            context.Response.Headers.Add(DgpCorrelationConstants.DgpCorrelationHeader, correlationContext.DgpHeader);
            await _next.Invoke(context);
        }

        private CorrelationContext GetCorrelationContextFromHeader(StringValues correlationHeaderValue)
        {
            CorrelationContext correlationContext = null;
            try
            {
                var json = Encoding.UTF8.GetString(Convert.FromBase64String(correlationHeaderValue.First()));
                correlationContext = JsonConvert.DeserializeObject<CorrelationContext>(json);
            }
            catch (System.Exception)
            {
                //ignore
            }

            return correlationContext;
        }

        private CorrelationContext CreateNewCorrelationContext(HttpContext context, Guid newCorrelationId)
        {
            return new CorrelationContext
            {
                Id = newCorrelationId,
                SourceId = _applicationContext.ApplicationId,
                SourceName = _applicationContext.ApplicationName,
                InstanceId = _applicationContext.InstanceId,
                InstanceName = _applicationContext.InstanceName,
                IpAddress = context.Connection?.RemoteIpAddress?.ToString() ?? string.Empty
            };
        }

        private static void SerializeHeader(CorrelationContext correlationContext)
        {
            correlationContext.DgpHeader =
                Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(correlationContext)));
        }
    }
}