using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using Digipolis.Errors;
using Digipolis.Errors.Exceptions;
using Digipolis.ServiceAgents;
using Newtonsoft.Json;

namespace Utilities.Exceptions
{
    public class ApiExceptionMapper : ExceptionMapper
    {
        private const string GenericErrorDueToExternalThings =
            "Error while calling an external service, no extra information available";

        protected override void Configure()
        {
            CreateMap<UnauthorizedAccessException>((error, ex) =>
            {
                var msg = string.Join(" ", "Access denied.", ex.Message, ex.InnerException?.Message);
                error.Title = msg;
                error.Code = "UNAUTH001";
                error.Status = (int) HttpStatusCode.Unauthorized;
            });

            CreateMap<BaseException>((error, ex) =>
            {
                var msg = string.Join(" -- ", ex.Messages.SelectMany(x => x.Value));
                if (string.IsNullOrWhiteSpace(msg) && ex is ServiceAgentException)
                    msg =
                        "Error while calling an external service and did not get more detail from the related service";

                error.Title = !string.IsNullOrWhiteSpace(msg) ? msg : GenericErrorDueToExternalThings;
                error.Code = string.IsNullOrWhiteSpace(ex.Code) ? "SAERROR001" : ex.Code;
                error.Status = (int) HttpStatusCode.InternalServerError;
            });
            CreateMap<ServiceAgentException>((error, ex) =>
            {
                error.Title = "ServiceAgent Exception";
                error.Code = "EXTERNALSERVICE001";
                error.Status = (int) HttpStatusCode.InternalServerError;
                UpdateServiceAgentErrorWithDetails(error, ex);
            });
            CreateMap<HttpRequestException>((error, ex) =>
            {
                var msg = string.Join(" -- ", ex.Message, ex.InnerException?.Message);
                error.Title = !string.IsNullOrWhiteSpace(msg) ? msg : GenericErrorDueToExternalThings;
                error.Code = "SAERROR002";
                error.Status = (int) HttpStatusCode.InternalServerError;
            });
            CreateMap<ArgumentException>((error, ex) =>
            {
                var msg = string.Join(" ", ex.Message, ex.InnerException?.Message);
                error.Title = !string.IsNullOrWhiteSpace(msg) ? msg : "Invalid argument was given.";
                error.Code = "ARGERROR001";
                error.Status = (int) HttpStatusCode.BadRequest;
            });
        }

        protected override void CreateDefaultMap(Error error, System.Exception exception)
        {
            error.Title = !string.IsNullOrWhiteSpace(exception.Message)
                ? exception.Message
                : "We are experiencing some technical difficulties.";
            error.Status = (int) HttpStatusCode.InternalServerError;
        }

        protected override void CreateNotFoundMap(Error error, NotFoundException exception)
        {
            if (!string.IsNullOrEmpty(exception.Code) && !string.IsNullOrWhiteSpace(exception.Message))
            {
                error.Title = exception.Message;
                error.Code = exception.Code;
            }
            else
            {
                error.Title = "Not found.";
                error.Code = "NOTFOUND001";
            }

            error.Status = (int) HttpStatusCode.NotFound;
        }


        private static bool UpdateServiceAgentErrorWithDetails(Error error, BaseException exception)
        {
            if (exception.Messages.ContainsKey("json"))
            {
                var message = exception.Messages["json"].FirstOrDefault();
                if (message != null)
                    try
                    {
                        var err = JsonConvert.DeserializeObject<Error>(message);
                        error.Code = "EXTERNALSERVICE001";
                        error.Status = err.Status;
                        error.Title = err.Title;
                        return true;
                    }
                    catch
                    {
                        //do nothing
                    }
            }

            return false;
        }
    }
}