using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Utilities.Exceptions
{
    public class GeneralExceptionFilter : ActionFilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            var logger = context.HttpContext.RequestServices.GetService<ILogger<GeneralExceptionFilter>>();
            logger.LogError(context.Exception, "Exception Filter");
        }
    }
}