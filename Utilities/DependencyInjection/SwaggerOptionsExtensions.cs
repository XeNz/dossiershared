using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Utilities.DependencyInjection
{
    public static class SwaggerOptionsExtensions
    {
        public static void  AddXmlDocsFromCurrentAssembly(this SwaggerGenOptions swaggerGenOptions)
        {
            var xmlDocs = GetXmlDocPaths(Assembly.GetExecutingAssembly());
            Array.ForEach(xmlDocs, d => swaggerGenOptions.IncludeXmlComments(d));
        }

        private static string[] GetXmlDocPaths(Assembly currentAssembly)
        {
            return currentAssembly.GetReferencedAssemblies()
                .Union(new[] {currentAssembly.GetName()})
                .Select(a => Path.Combine(Path.GetDirectoryName(currentAssembly.Location), $"{a.Name}.xml"))
                .Where(File.Exists)
                .ToArray();
        }
    }
}