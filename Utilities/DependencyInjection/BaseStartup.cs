using System;
using Digipolis.ApplicationServices;
using Digipolis.ServiceAgents;
using Digipolis.ServiceAgents.OAuth;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Utilities.Correlations;
using Utilities.Logging;
using Utilities.Options;
using Digipolis.Web;
using Digipolis.Web.Api.Tools;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Utilities.DelegatingHandlers;
using Utilities.Exceptions;

namespace Utilities.DependencyInjection
{
    public abstract class BaseStartup
    {
        public BaseStartup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; set; }

        public virtual void
            ConfigureHangfireServices(IServiceCollection services, DatabaseSettings databaseSettings) =>
            throw new NotImplementedException();

        public virtual void
            ConfigureDatabaseServices(IServiceCollection services, DatabaseSettings databaseSettings) =>
            throw new NotImplementedException();

        /// <summary>
        /// Adds several helper services to the IServiceCollection
        /// </summary>
        /// <example>
        /// <para> Digipolis specific services like TokenHelper and RequestHeaderHelper </para>
        /// <para> Commonly used handlers such as: LoggingHandler, ExceptionHandler, AddCorrelationHandler and PassThroughUserAuthTokenHandler</para>
        /// </example>
        /// <param name="services"></param>
        public virtual void ConfigureHelperServices(IServiceCollection services)
        {
            services.AddScoped<ITokenHelper, TokenHelper>();
            services.AddScoped<IRequestHeaderHelper, RequestHeaderHelper>();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IUrlHelper>(x => new UrlHelper(x.GetService<IActionContextAccessor>().ActionContext));
            services.AddScoped<ILinkProvider, LinkProvider>();


            services.TryAddTransient(typeof(LoggingHandler<>));
            services.TryAddTransient(typeof(ExceptionHandler<>));

            services.TryAddTransient<AddCorrelationHandler>();
            services.TryAddTransient<PassThroughUserAuthTokenHandler>();
        }

        public virtual void ConfigureAutoMapper(IServiceCollection services) => throw new NotImplementedException();

        public virtual void ConfigureBusinessServices(IServiceCollection services) =>
            throw new NotImplementedException();

        /// <summary>
        /// <para> Reads out and sets basic database, app, gdp and hangfire configuration to bind to IOptions</para>
        /// <para> Sets global JSON serializer settings</para>
        /// <para> Sets application Id and Application Name</para>
        /// <para> Registers ApiExceptionMapper</para>
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public virtual IServiceCollection ConfigureServices(IServiceCollection services)
        {
            var databaseSettings = new DatabaseSettings();
            Configuration.GetSection("DataAccess:ConnectionString").Bind(databaseSettings);
            services.AddCorrelations();
            services.AddEnrichers();
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"))
                .Configure<GdpSettings>(Configuration.GetSection("GdpSettings"))
                .Configure<DatabaseSettings>(Configuration.GetSection("DataAccess:ConnectionString"))
                .Configure<HangfireSettings>(Configuration.GetSection("HangfireSettings"));


            services.AddMemoryCache();

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            services.AddMvc(options => options.EnableEndpointRouting = false)
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    options.SerializerSettings.Converters.Add(new StringEnumConverter());
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddControllersAsServices()
                .AddApiExtensions(null, options => { options.DisableVersioning = true; });

            var appSettings = services.BuildServiceProvider().GetService<IOptions<AppSettings>>().Value;
            services.AddApplicationServices(opt =>
            {
                opt.ApplicationId = appSettings.ApplicationId;
                opt.ApplicationName = appSettings.AppName;
            });

            services.AddGlobalErrorHandling<ApiExceptionMapper>();
            return services;
        }
    }
}