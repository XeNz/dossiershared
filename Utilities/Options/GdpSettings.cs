using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Utilities.Options
{
    [ExcludeFromCodeCoverage]
    public class GdpSettings
    {
        public string Version { get; set; }
        public string Prefix { get; set; }
        public Dictionary<string, string> DossierTypes { get; set; }
        public Dictionary<string, string> DataTypes { get; set; }
    }
}