﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Utilities.Options
{
    [ExcludeFromCodeCoverage]
    public static class EnvironmentExtensions
    {
        public const string ServiceAgentRoot = "SERVICEAGENTS";
        public const string DatabaseRoot = "DB_POSTGRESQL";

        public static string GetVariableFromEnv(this string variableName, string fallback = null)
        {
            var value = Environment.GetEnvironmentVariable(variableName.ToUpperInvariant());
            return value ?? fallback;
        }

        public static ushort GetVariableFromEnv(this string variableName, ushort fallback)
        {
            var value = Environment.GetEnvironmentVariable(variableName.ToUpperInvariant());
            ushort.TryParse(value, out var cast);
            return cast == 0 ? fallback : cast;
        }
    }
}