using System;

namespace Utilities.Options
{
    public class ProcessSettings
    {
        public string BaseUrl { get; set; }
        public Guid ApiKey { get; set; }
        public string ProcessDefinitionKey { get; set; }
    }
}