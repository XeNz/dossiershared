using System.Diagnostics.CodeAnalysis;

namespace Utilities.Options
{
    [ExcludeFromCodeCoverage]
    public class AppSettings
    {
        public string AppName { get; set; }
        public string ApplicationId { get; set; }
        public string LoggerIndex { get; set; }
        public string LoggerApplicationName { get; set; }
        public string LoggerApplicationId { get; set; }
    }
}