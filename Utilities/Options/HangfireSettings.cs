namespace Utilities.Options
{
    public class HangfireSettings
    {
        /// <summary>
        ///     The header used to get access to the hangfire dashboard
        /// </summary>
        public string HangfireAuthKey { get; set; }

        /// <summary>
        ///     Amount of workers Hangfire used
        /// </summary>
        public int HangfireWorkers { get; set; }
    }
}