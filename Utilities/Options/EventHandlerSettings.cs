﻿namespace Utilities.Options
{
    public class EventHandlerSettings
    {
        /// <summary>
        ///     DateTime treshold set in minutes
        /// </summary>
        /// <example>
        ///     DateTimeTreshold = 1440;
        ///     and CreatedDate = 04/10/2018
        ///     means that the upper end will be 05/10/2018
        ///     and that the lowerend will be 03/10/2018
        /// </example>
        public int DateTimeTreshold { get; set; }

        /// <summary>
        ///     Geo distance treshold set in meters
        /// </summary>
        public int GeoDistanceTreshold { get; set; }
    }
}