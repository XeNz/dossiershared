using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Utilities.DelegatingHandlers
{
    /// <summary>
    /// Needed if if there is an endpoint with restricted access and where a user token is enforced
    /// </summary>
    public class PassThroughUserAuthTokenHandler : DelegatingHandler
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private const string Authorization = "Authorization";

        public PassThroughUserAuthTokenHandler(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (_httpContextAccessor.HttpContext != null)
            {
                var token = _httpContextAccessor.HttpContext.Request.Headers.FirstOrDefault(x =>
                    string.Equals(x.Key, "Authorization", StringComparison.OrdinalIgnoreCase)).Value.FirstOrDefault();

                if (!string.IsNullOrEmpty(token))
                {
                    if (request.Headers.Contains(Authorization))
                        request.Headers.Remove(Authorization);
                    request.Headers.TryAddWithoutValidation(Authorization, $"{token}");
                }
            }

            return await base.SendAsync(request, cancellationToken);
        }
    }
}