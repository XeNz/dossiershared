﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Utilities.DelegatingHandlers
{
    public class ExceptionHandler<T> : DelegatingHandler
    {
        private readonly ILogger<T> _logger;

        public ExceptionHandler(ILogger<T> logger)
        {
            _logger = logger;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            try
            {
                return await base.SendAsync(request, cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"{nameof(T)} Exception");
                throw;
            }
        }
    }
}
