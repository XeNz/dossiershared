using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Utilities.Correlations.Model;

namespace Utilities.DelegatingHandlers
{
    public class AddCorrelationHandler : DelegatingHandler
    {
        private IHttpContextAccessor _context;

        public AddCorrelationHandler(IHttpContextAccessor context)
        {
            _context = context;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            if (!request.Headers.Contains(DgpCorrelationConstants.DgpCorrelationHeader))
            {
                if (_context.HttpContext != null &&
                    _context.HttpContext.Items[DgpCorrelationConstants.DgpCorrelationHeader] is CorrelationContext
                        dgpHeaderValue)
                {
                    var correlation =
                        Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(dgpHeaderValue)));
                    request.Headers.TryAddWithoutValidation(DgpCorrelationConstants.DgpCorrelationHeader, correlation);
                }
            }

            return await base.SendAsync(request, cancellationToken);
        }
    }
}