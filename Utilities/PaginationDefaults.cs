﻿namespace Utilities
{
    public static class PaginationDefaults
    {
        public const int DefaultFirstPage = 1;
        public const int DefaultPageSize = 10;
    }
}