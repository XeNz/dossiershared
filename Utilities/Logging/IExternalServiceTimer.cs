﻿using System;

namespace Utilities.Logging
{
    public interface IExternalServiceTimer
    {
        void AddTimeSpan(TimeSpan timeSpent);
        TimeSpan Calculate();
    }
}