using System;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Serilog;
using Serilog.Core;
using Serilog.Debugging;

namespace Utilities.Logging
{
    public static class LogExtensions
    {
        public static void ConfigureLogging(this IApplicationBuilder app, IConfiguration configuration)
        {
#if DEBUG
            SelfLog.Enable(Console.Error);
#endif
            var logConfiguration = new LoggerConfiguration()
#if DEBUG
                .MinimumLevel.Debug()
                .WriteTo.Logger(l => l.ReadFrom.ConfigurationSection(configuration.GetSection("DEVELOPMENTLOG")))
#else
                .MinimumLevel.Information()
                .MinimumLevel.Override("System", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.EntityFrameworkCore.Database.Command", LogEventLevel.Warning)
                .WriteTo.Logger(l => l.ReadFrom.ConfigurationSection(configuration.GetSection("APPLICATIONLOG")))
#endif
                .Enrich.With(app.ApplicationServices.GetServices<ILogEventEnricher>().ToArray())
                .Enrich.FromLogContext();
            Log.Logger = logConfiguration.CreateLogger();
            Console.WriteLine(JsonConvert.SerializeObject(configuration.GetSection("APPLICATIONLOG").AsEnumerable()));
        }

        public static void AddEnrichers(this IServiceCollection services)
        {
            services.AddTransient<IExternalServiceTimer, ExternalServiceTimer>();
            services.AddSingleton<ILogEventEnricher, MessageEnricher>();
        }

        public static IApplicationBuilder UseRequestLogMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RequestLogMiddleware>();
        }
    }
}