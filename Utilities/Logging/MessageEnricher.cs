using System;
using Digipolis.ApplicationServices;
using Microsoft.AspNetCore.Http;
using Serilog.Core;
using Serilog.Events;
using Utilities.Correlations.Model;

namespace Utilities.Logging
{
    public class MessageEnricher : ILogEventEnricher
    {
        private readonly LogEventProperty _applicationIdProperty;
        private readonly LogEventProperty _applicationNameProperty;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly LogEventProperty _instanceIdProperty;
        private readonly LogEventProperty _instanceNameProperty;
        private readonly LogEventProperty _machineNameProperty;
        private readonly LogEventProperty _messageVersionProperty;

        public MessageEnricher(IHttpContextAccessor contextAccessor, IApplicationContext applicationContext)
        {
            _contextAccessor = contextAccessor;
            _messageVersionProperty =
                new LogEventProperty(MessageProperties.MessageVersionProperty, new ScalarValue(1));
            _applicationIdProperty = new LogEventProperty(MessageProperties.ApplicationId,
                new ScalarValue(applicationContext?.ApplicationId ?? MessageProperties.NullValue));
            _applicationNameProperty = new LogEventProperty(MessageProperties.ApplicationName,
                new ScalarValue(applicationContext?.ApplicationName ?? MessageProperties.NullValue));
            _instanceIdProperty = new LogEventProperty(MessageProperties.InstanceId,
                new ScalarValue(applicationContext?.InstanceId ?? MessageProperties.NullValue));
            _instanceNameProperty = new LogEventProperty(MessageProperties.InstanceName,
                new ScalarValue(applicationContext?.InstanceName ?? MessageProperties.NullValue));
            _machineNameProperty = new LogEventProperty(MessageProperties.ApplicationMachineName,
                new ScalarValue(Environment.MachineName));
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            logEvent.RemovePropertyIfPresent("messageTemplate");

            CorrelationContext correlationContext = null;
            if (_contextAccessor.HttpContext != null &&
                _contextAccessor.HttpContext.Items.TryGetValue(DgpCorrelationConstants.DgpCorrelationHeader,
                    out var correlationData))
                correlationContext = correlationData as CorrelationContext;

            logEvent.AddOrUpdateProperty(new LogEventProperty(MessageProperties.CorrelationId,
                new ScalarValue(correlationContext?.Id != null
                    ? correlationContext.Id.Value.ToString()
                    : MessageProperties.NullValue)));
            logEvent.AddOrUpdateProperty(new LogEventProperty(MessageProperties.CorrelationSourceId,
                new ScalarValue(correlationContext?.SourceId ?? MessageProperties.NullValue)));
            logEvent.AddOrUpdateProperty(new LogEventProperty(MessageProperties.CorrelationSourceName,
                new ScalarValue(correlationContext?.SourceName ?? MessageProperties.NullValue)));
            logEvent.AddOrUpdateProperty(new LogEventProperty(MessageProperties.CorrelationInstanceId,
                new ScalarValue(correlationContext?.InstanceId ?? MessageProperties.NullValue)));
            logEvent.AddOrUpdateProperty(new LogEventProperty(MessageProperties.CorrelationInstanceName,
                new ScalarValue(correlationContext?.InstanceName ?? MessageProperties.NullValue)));
            logEvent.AddOrUpdateProperty(new LogEventProperty(MessageProperties.CorrelationHostName,
                new ScalarValue(correlationContext?.IpAddress ?? MessageProperties.NullValue)));


            logEvent.AddOrUpdateProperty(_messageVersionProperty);
            logEvent.AddOrUpdateProperty(_applicationIdProperty);
            logEvent.AddOrUpdateProperty(_applicationNameProperty);
            logEvent.AddOrUpdateProperty(_instanceIdProperty);
            logEvent.AddOrUpdateProperty(_instanceNameProperty);
            logEvent.AddOrUpdateProperty(_machineNameProperty);
        }
    }
}