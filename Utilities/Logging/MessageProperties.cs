namespace Utilities.Logging
{
    public static class MessageProperties
    {
        public const string NullValue = "null";

        public const string MessageVersionProperty = "MessageVersion";
        public const string CorrelationId = "CorrelationId";
        public const string CorrelationSourceId = "CorrelationSourceId";
        public const string CorrelationSourceName = "CorrelationSourceName";
        public const string CorrelationInstanceId = "CorrelationInstanceId";
        public const string CorrelationInstanceName = "CorrelationInstanceName";
        public const string CorrelationHostName = "CorrelationHostName";

        public const string ApplicationId = "ApplicationId";
        public const string ApplicationName = "ApplicationName";
        public const string InstanceId = "ApplicationInstanceId";
        public const string InstanceName = "ApplicationInstanceName";
        public const string ApplicationMachineName = "ApplicationMachineName";
    }
}