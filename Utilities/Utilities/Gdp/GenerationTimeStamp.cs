﻿using System;

namespace Utilities.Utilities.Gdp
{
    public class GenerationTimeStamp
    {
        public DateTime TimeStamp { get; set; }
        public int Duration { get; set; }
    }
}