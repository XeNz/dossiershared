﻿using System.Collections.Generic;
using Digipolis.Errors.Exceptions;

namespace Utilities.Utilities.Gdp
{
    public class GdpException : BaseException
    {
        public GdpException(string message = null, string code = null, System.Exception exception = null,
            Dictionary<string, IEnumerable<string>> messages = null) : base(message, code, exception, messages)
        {
        }
    }
}