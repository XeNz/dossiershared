﻿using System;
using System.Collections.Generic;

namespace Utilities.Utilities.Gdp
{
    public class GdpError
    {
        public Guid Identifier { get; set; }
        public string Title { get; set; }
        public string Self { get; set; }
        public GenerationTimeStamp Generation { get; set; }
        public IEnumerable<FeedbackItem> Feedback { get; set; }
        public int Status { get; set; }
    }
}