﻿namespace Utilities.Utilities.Gdp
{
    public class FeedbackItem
    {
        public string Type { get; set; }
        public string Description { get; set; }
    }
}